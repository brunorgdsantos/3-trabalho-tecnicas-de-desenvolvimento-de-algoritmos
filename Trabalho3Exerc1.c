#include<stdio.h>

int main(){

    char var1[20], var2[20]; //Reservando espa�o para 12 caracteres em var1 e var2;
    int contador = 0; //Vari�vel que ser� usada no controle do loop e nos indices das variaveis;

    printf("Digite algo:");
    gets(var1); //gets permite atribuir strings a vari�vel mesmo se houver espa�os;

    for(contador = 0; contador<20; contador++){ //Estabelecendo o inicio e o fim dos lopp, como tamb�m seu inscremento
        var2[contador] = var1[contador]; //Aqui usamos o incremente do contador como indice
    }
    printf("Var1: %s \n Var2: %s", var1, var2);
    return 0;
}
